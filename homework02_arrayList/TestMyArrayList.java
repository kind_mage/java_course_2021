package homework02_arrayList;

public class TestMyArrayList {

    public static void main(String[] args){
        MyArrayList<String> strings = new MyArrayList();

        strings.add("Hello!");
        strings.add("Marsel");
        strings.add("Bye");
        strings.add("Java");
        strings.add("C++");
        strings.add("Python");
        strings.add("Hello!");
        strings.add("Marsel");
        strings.add("Bye");
        strings.add("Java");
        strings.add("C++");
        strings.add("Python");
        strings.add("Hello!");
        strings.add("Marsel");
        strings.add("Bye");
        strings.add("Java");
        strings.add("C++");
        strings.add("Python");

        System.out.println(strings.size());
        System.out.println(strings.get(3));
        System.out.println("------------------------");

        Iterator iterator = strings.iterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());

        }

    }
}
