package homework02_arrayList;

public class MyArrayList<E> implements List<E>{
    public static final int INITIAL_CAPACITY = 10;
    private E[] data = (E[]) new Object[INITIAL_CAPACITY];
    private int size = 0;


    @Override
    public void add(E element) {
        add(size, element);
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return data[index];
    }
    public void checkIndex(int index){
        if (index < 0 && index > size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
    }


    public void add(int index, E e) {
        if (index < 0 || index > size)
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        ensureCapacity();

        for (int i = size - 1; i >= index; i--)
            data[i + 1] = data[i];

        data[index] = e;

        size++;
    }

    public void ensureCapacity() {
        if (size >= data.length) {
            E[] newData = (E[]) (new Object[size * 2 + 1]);
            System.arraycopy(data, 0, newData, 0, size);
            data = newData;
        }
    }
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    public class ArrayListIterator implements Iterator<E> {
        int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public E next() {
            return data[current++];
        }
    }

}
