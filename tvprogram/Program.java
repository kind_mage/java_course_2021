package tvprogram;

public class Program {
	
	
	
	public static void main(String[] args){
		TV tv = new TV("Samsung");
		Channel channel = new Channel("Первый");
		Programs p1 = new Programs("Давай поженимся");
		Programs p2 = new Programs("Малахов+");
		Programs p3 = new Programs("Пусть говорят");
		
		Controller controller = new Controller(tv);
		
		tv.addChammel(channel);
		channel.addProgram(p1);
		channel.addProgram(p2);
		channel.addProgram(p3);
		
		controller.on(1);
		
	}

}
